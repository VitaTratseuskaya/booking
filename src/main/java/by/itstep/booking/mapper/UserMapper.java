package by.itstep.booking.mapper;

import by.itstep.booking.dto.user.*;
import by.itstep.booking.entity.UserEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserEntity map(UserCreateDto dto);

    UserFullDto map(UserEntity entity);

    List<UserFullDto> map(List<UserEntity> entities);

    UserEntity map(UserUpdateDto dto);


}
