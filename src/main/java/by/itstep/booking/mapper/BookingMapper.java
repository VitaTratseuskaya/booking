package by.itstep.booking.mapper;

import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.booking.BookingFullDto;
import by.itstep.booking.entity.BookingEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BookingMapper {

    BookingEntity map(BookingCreateDto dto);

    BookingFullDto map(BookingEntity entity);

    List<BookingFullDto> map(List<BookingEntity> entities);

}
