package by.itstep.booking.mapper;

import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.dto.country.CountryShortDto;
import by.itstep.booking.entity.CountryEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CountryMapper {

    CountryEntity map(CountryCreateDto dto);

    CountryFullDto map(CountryEntity entity);

    List<CountryFullDto> map(List<CountryEntity> entities);

    List<CountryShortDto> mapToShort(List<CountryEntity> entities);
}
