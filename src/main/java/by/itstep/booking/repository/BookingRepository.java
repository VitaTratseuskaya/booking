package by.itstep.booking.repository;

import by.itstep.booking.entity.BookingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.Instant;
import java.util.List;

public interface BookingRepository extends JpaRepository<BookingEntity, Integer> {

    @Query(nativeQuery = true, value = "SELECT * FROM users " +
            "LEFT JOIN bookings USING (user_id) " +
            "WHERE user_id = :userId AND booking_id = :bookingId")
    BookingEntity foundBooking(@Param("userId") Integer userId,
                               @Param("bookingId") Integer bookingId);

    @Query(nativeQuery = true, value = "SELECT * FROM bookings WHERE room_id = :roomId")
    List<BookingEntity> findBookingsByRoomId(@Param("roomId") Integer roomId);

    @Query(nativeQuery = true, value = "SELECT * FROM bookings WHERE user_id = :userId")
    List<BookingEntity> findBookingsByUserId(@Param("userId") Integer userId);

    @Query(nativeQuery = true, value = "SELECT DATEDIFF( :finishDate, :startDate) FROM booking.bookings")
    int daysInReservation(@Param("finishDate") Instant finishDate,
                          @Param("startDate") Instant startDate);


}
