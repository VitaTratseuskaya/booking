package by.itstep.booking.repository;

import by.itstep.booking.entity.RoomEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.Instant;
import java.util.List;

public interface RoomRepository extends JpaRepository<RoomEntity, Integer> {

    @Query(nativeQuery = true, value = "SELECT DISTINCT rooms.room_id FROM rooms " +
            "LEFT JOIN bookings ON bookings.room_id = rooms.room_id " +
            "WHERE hotel_id = :hotelId AND quantity_bed = :quantityBed " +
            "AND (start_date <= :startDate OR finish_date >= :finishDate) " +
            "AND (:startDate AND :finishDate NOT BETWEEN start_date and finish_date)")
    List<Integer> findByFreeRoom(@Param("hotelId") int hotelId,
                                 @Param("quantityBed") int quantityBed,
                                 @Param("startDate") Instant startDate,
                                 @Param("finishDate") Instant finishDate);

    @Query(nativeQuery = true, value = "SELECT DISTINCT rooms.room_id FROM rooms " +
            "LEFT JOIN bookings ON bookings.room_id = rooms.room_id " +
            "WHERE (:startDate AND :finishDate  BETWEEN start_date and finish_date)")
    List<Integer> research(@Param("startDate") Instant startDate,
                           @Param("finishDate") Instant finishDate);

    @Query(nativeQuery = true, value = "SELECT * FROM rooms " +
            "JOIN hotels USING (hotel_id) " +
            "WHERE hotel_id = :hotelId")
    List<RoomEntity> findHotelWithRoom(@Param("hotelId") Integer hotelId);

}
