package by.itstep.booking.repository;

import by.itstep.booking.dto.user.UserChangePasswordDto;
import by.itstep.booking.dto.user.UserFullDto;
import by.itstep.booking.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    Optional<UserEntity> findByEmail(String email);


    @Query(nativeQuery = true, value = "SELECT password FROM users WHERE user_id = :userId and deleted_at IS NULL")
    String findPassword(@Param("userId") Integer userId);






}
