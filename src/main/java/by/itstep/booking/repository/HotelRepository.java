package by.itstep.booking.repository;

import by.itstep.booking.entity.HotelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.Instant;
import java.util.List;

public interface HotelRepository extends JpaRepository<HotelEntity, Integer> {

    @Query(nativeQuery = true, value = "SELECT * FROM hotels " +
            "JOIN rooms ON hotels.hotel_id  " +
            "JOIN bookings ON rooms.room_id " +
            "WHERE hotels.country_id = :countryId AND rooms.quantity_bed = :quantityBed " +
            "AND (start_date <= :startDate OR finish_date >= :finishDate) " +
            "AND (:startDate AND :finishDate NOT BETWEEN start_date and finish_date) or hotels.hotel_id is null " +
            "GROUP BY hotels.hotel_id")
    List<HotelEntity> findAllHotelInCountry(@Param("finishDate") Instant finishDate,
                                            @Param("startDate") Instant startDate,
                                            @Param("countryId") int countryId,
                                            @Param("quantityBed") int quantityBed);
}
