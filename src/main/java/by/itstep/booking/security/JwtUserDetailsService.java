package by.itstep.booking.security;

import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        UserEntity user = userRepository
                .findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException
                        ("JwtUserDetailService -> user was not found exception: " + email));

        return new JwtUserDetails(
                user.getEmail(),
                user.getPassword(),
                user.getBlocked(),
                Arrays.asList(user.getRole()));
    }
}
