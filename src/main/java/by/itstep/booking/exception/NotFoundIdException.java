package by.itstep.booking.exception;

public class NotFoundIdException extends RuntimeException {

    public NotFoundIdException(String message) {
        super(message);
    }
}
