package by.itstep.booking.exception;

public class InvalidDateBookingException extends RuntimeException{

    public InvalidDateBookingException(String message) {
        super(message);
    }
}
