package by.itstep.booking.service;

import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface RoomService {

    RoomFullDto create(RoomCreateDto dto);

    RoomFullDto update(RoomUpdateDto dto);

    RoomFullDto findById(Integer id);

    Page<RoomFullDto> findAll(int size, int page);

    void delete(Integer id);
}
