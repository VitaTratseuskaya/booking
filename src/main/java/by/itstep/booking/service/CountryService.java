package by.itstep.booking.service;

import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CountryService {

    CountryFullDto create(CountryCreateDto dto);

    CountryFullDto update(CountryUpdateDto dto);

    CountryFullDto findById(Integer id);

    Page<CountryFullDto> findAll(int page, int size);

    void delete(Integer id);
}
