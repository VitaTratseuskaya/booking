package by.itstep.booking.service;

import by.itstep.booking.dto.user.*;
import org.springframework.data.domain.Page;

import java.util.List;

public interface UserService {

    UserFullDto create(UserCreateDto dto);

    UserFullDto update(UserUpdateDto dto);

    UserFullDto findById (int id);

    Page<UserFullDto> findAll(int page, int size);

    void delete(Integer id);

    UserFullDto changeRole(UserChangeRoleDto dto);

    void changePassword(UserChangePasswordDto dto);

    void block(Integer userId);

}
