package by.itstep.booking.service;

import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.booking.BookingFullDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface BookingService {

    BookingFullDto create(BookingCreateDto dto);

    BookingFullDto findById(Integer id);

    Page<BookingFullDto> findAll(int page, int size);

    void delete(Integer id);

    void deleteBooking(Integer bookingId);
}
