package by.itstep.booking.service;

import by.itstep.booking.dto.hotel.*;
import org.springframework.data.domain.Page;

import java.util.List;

public interface HotelService {

    HotelFullDto create(HotelCreateDto dto);

    HotelFullDto update(HotelUpdateDto dto);

    HotelFullDto findById(Integer id);

    Page<HotelFullDto> findAll(int page, int size);

    void delete(Integer id);

    List<HotelShortDto> findAllHotelInCountry(HotelFoundDto dto);
}
