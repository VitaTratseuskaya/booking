package by.itstep.booking.service.implementation;

import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import by.itstep.booking.entity.BookingEntity;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.mapper.RoomMapper;
import by.itstep.booking.repository.BookingRepository;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.repository.RoomRepository;
import by.itstep.booking.service.BookingService;
import by.itstep.booking.service.RoomService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class RoomServiceImpl implements RoomService {

    private final RoomMapper roomMapper;
    private final RoomRepository roomRepository;
    private final HotelRepository hotelRepository;
    private final BookingRepository bookingRepository;
    private final BookingService bookingService;


    @Override
    @Transactional
    public RoomFullDto create(RoomCreateDto dto) {
        RoomEntity roomToSave = roomMapper.map(dto);

        HotelEntity hotel = hotelRepository
                .findById(dto.getHotelId())
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getHotelId()));
        roomToSave.setHotel(hotel);

        roomToSave.setNumber(dto.getNumber());
        roomToSave.setQuantityBed(dto.getQuantityBed());
        roomToSave.setPrice(payment(hotel, roomToSave));

        RoomEntity savedRoom = roomRepository.save(roomToSave);
        log.info("Room was successfully created");

        return roomMapper.map(savedRoom);
    }

    @Override
    @Transactional
    public RoomFullDto update(RoomUpdateDto dto) {
        RoomEntity roomForUpdate = roomRepository
                .findById(dto.getRoomId())
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + dto.toString()));

        roomForUpdate.setRoomId(dto.getRoomId());
        roomForUpdate.setQuantityBed(dto.getQuantityBed());
        roomForUpdate.setNumber(dto.getNumber());

        RoomEntity updatedRoom = roomRepository.save(roomForUpdate);
        log.info("Room was successfully updated");

        return roomMapper.map(updatedRoom);
    }

    @Override
    @Transactional
    public RoomFullDto findById(Integer id) {
        RoomEntity room = roomRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));

        log.info("Found room " + room);
        return roomMapper.map(room);
    }

    @Override
    @Transactional
    public Page<RoomFullDto> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<RoomEntity> foundRooms = roomRepository.findAll(pageable);
        Page<RoomFullDto> dtos = foundRooms.map(entity -> roomMapper.map(entity));
        log.info("Found rooms -> " + dtos.getNumberOfElements());
        return dtos;
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        RoomEntity room = roomRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));

        List<BookingEntity> bookings = bookingRepository.findBookingsByRoomId(room.getRoomId());
        room.setBookings(bookings);

        for (BookingEntity booking : bookings) {
            bookingService.delete(booking.getBookingId());
        }

        room.setDeletedAt(Instant.now());
        roomRepository.save(room);

        log.info("Room was successfully deleted");
    }


    private double payment(HotelEntity hotel, RoomEntity room) {
        double result = 0.0;
        if (room.getQuantityBed() == 1) {
            result = hotel.getPrice() * 1.0;
            room.setPrice(result);
        }
        if (room.getQuantityBed() == 2) {
            result = hotel.getPrice() * 1.25;
            room.setPrice(result);
        }
        if (room.getQuantityBed() == 3) {
            result = hotel.getPrice() * 1.5;
            room.setPrice(result);
        }
        if (room.getQuantityBed() == 4) {
            result = hotel.getPrice() * 1.75;
            room.setPrice(result);
        }
        return result;
    }
}
