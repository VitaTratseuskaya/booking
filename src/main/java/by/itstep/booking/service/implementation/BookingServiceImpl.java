package by.itstep.booking.service.implementation;

import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.booking.BookingFullDto;
import by.itstep.booking.entity.BookingEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.exception.NotEnoughMoneyException;
import by.itstep.booking.mapper.BookingMapper;
import by.itstep.booking.repository.BookingRepository;
import by.itstep.booking.repository.RoomRepository;
import by.itstep.booking.security.AuthenticationService;
import by.itstep.booking.service.BookingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookingServiceImpl implements BookingService {

    private final BookingMapper bookingMapper;
    private final RoomRepository roomRepository;
    private final BookingRepository bookingRepository;
    private final AuthenticationService authenticationService;

    @Override
    @Transactional
    public BookingFullDto create(BookingCreateDto dto) {
        UserEntity userOnline = authenticationService.getAuthenticationUser();

        BookingEntity bookingToSave = bookingMapper.map(dto);
        bookingToSave.setUser(userOnline);
        bookingToSave.setRoom(checkDate(dto));

        pay(userOnline, bookingToSave.getRoom(), bookingToSave);

        BookingEntity savedBooking = bookingRepository.save(bookingToSave);
        log.info("Booking was successfully created! Your room №" + bookingToSave.getRoom().getRoomId());

        return bookingMapper.map(savedBooking);
    }

    @Override
    @Transactional
    public BookingFullDto findById(Integer id) {
        BookingEntity booking = bookingRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));


        log.info("Found booking " + booking);
        return bookingMapper.map(booking);
    }

    @Override
    @Transactional
    public Page<BookingFullDto> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<BookingEntity> foundBookings = bookingRepository.findAll(pageable);
        Page<BookingFullDto> dtos = foundBookings.map(entity -> bookingMapper.map(entity));
        log.info("Found bookings -> " + dtos.getNumberOfElements());
        return dtos;
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        BookingEntity booking = bookingRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));

        booking.setDeletedAt(Instant.now());
        bookingRepository.save(booking);

        log.info("Booking was successfully deleted");
    }

    @Override
    @Transactional
    public void deleteBooking(Integer bookingId) {
        UserEntity userOnline = authenticationService.getAuthenticationUser();
        BookingEntity booking = bookingRepository.foundBooking(userOnline.getUserId(), bookingId);
        int daysInReservation = bookingRepository.daysInReservation(booking.getFinishDate(), booking.getStartDate());
        double residingCost = booking.getRoom().getPrice() * daysInReservation;

        if (Instant.now().isAfter(booking.getStartDate())) {
            log.info("This order has already been closed.");
            booking.setDeletedAt(booking.getFinishDate().plusSeconds(86400));
            bookingRepository.save(booking);
        } else {
            userOnline.setWallet(userOnline.getWallet() + residingCost);
            booking.setDeletedAt(Instant.now());
            bookingRepository.save(booking);

            log.info(userOnline.getFirstName() + " " +
                    userOnline.getLastName() + " was successfully deleted booking.");
        }
    }

    @Transactional
    private RoomEntity checkDate(BookingCreateDto dto) {
        List<Integer> rooms = roomRepository.findByFreeRoom(
                dto.getHotelId(), dto.getQuantityBed(), dto.getStartDate(), dto.getFinishDate());
        RoomEntity freeRoom = new RoomEntity();

        List<Integer> refreeRoom = roomRepository.research(dto.getStartDate(), dto.getFinishDate());

        if (rooms.isEmpty() && refreeRoom.isEmpty()) {
            throw new AppEntityNotFoundException("Sorry!");
        }

        for (int room : rooms) {
            if (refreeRoom.contains(room)) {
                if (refreeRoom.size() == rooms.size()) {
                    throw new AppEntityNotFoundException("Sorry! Please, change date.");
                }
                break;
            }
            freeRoom = roomRepository.getById(room);
        }
        return freeRoom;
    }

    @Transactional
    private double pay(UserEntity onlineUser, RoomEntity room, BookingEntity booking) {
        int daysInReservation = bookingRepository.daysInReservation(booking.getFinishDate(), booking.getStartDate());

        if (onlineUser.getWallet() == null & onlineUser.getWallet() < room.getPrice() * daysInReservation) {
            throw new NotEnoughMoneyException("Not enough money in your card!");
        }

        double pay = onlineUser.getWallet() - (room.getPrice() * daysInReservation);
        onlineUser.setWallet(pay);
        log.info("Your price $ " + room.getPrice() * daysInReservation);

        return pay;
    }
}
