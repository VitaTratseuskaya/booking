package by.itstep.booking.service.implementation;

import by.itstep.booking.dto.user.*;
import by.itstep.booking.entity.BookingEntity;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.enums.Role;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.exception.UniqueValuesIsTakenException;
import by.itstep.booking.exception.WrongUserPasswordException;
import by.itstep.booking.mapper.UserMapper;
import by.itstep.booking.repository.BookingRepository;
import by.itstep.booking.repository.UserRepository;
import by.itstep.booking.security.AuthenticationService;
import by.itstep.booking.service.BookingService;
import by.itstep.booking.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final BookingRepository bookingRepository;
    private final BookingService bookingService;
    private final AuthenticationService authenticationService;

    @Value("${jwt.secretKey}")
    private String secret;
    @Override
    @Transactional
    public UserFullDto create(UserCreateDto dto) {
        UserEntity entityToSave = userMapper.map(dto);
        entityToSave.setBlocked(false);
        entityToSave.setRole(Role.USER);
        entityToSave.setWallet(0.0);
        entityToSave.setPassword(passwordEncoder.encode(dto.getPassword() + dto.getEmail()));

        Optional<UserEntity> entityWithSameEmail = userRepository.findByEmail(entityToSave.getEmail());
        if (entityWithSameEmail.isPresent()) {
            throw new UniqueValuesIsTakenException("Email is taken!");
        }

        UserEntity savedEntity = userRepository.save(entityToSave);
        log.info("User was successfully created");
        return userMapper.map(savedEntity);
    }

    @Override
    @Transactional
    public UserFullDto update(UserUpdateDto dto) {
        UserEntity userForUpdate = userRepository
                .findById(dto.getUserId())
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + dto.getUserId()));

        throwIfNotCurrentUserOrAdmin(userForUpdate.getEmail());

        userForUpdate.setUserId(dto.getUserId());
        userForUpdate.setFirstName(dto.getFirstName());
        userForUpdate.setLastName(dto.getLastName());
        userForUpdate.setPhone(dto.getPhone());

        UserEntity updatedUser = userRepository.save(userForUpdate);
        log.info("User was successfully updated");
        return userMapper.map(updatedUser);
    }

    @Override
    @Transactional
    public UserFullDto findById(int id) {
        UserEntity user = userRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));

        log.info("Found user " + user);
        return userMapper.map(user);
    }

    @Override
    @Transactional
    public Page<UserFullDto> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<UserEntity> foundUsers = userRepository.findAll(pageable);
        Page<UserFullDto> dtos = foundUsers.map(entity -> userMapper.map(entity));
        log.info("Found users -> " + dtos.getNumberOfElements());
        return dtos;
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        UserEntity foundUser = userRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));

        List<BookingEntity> bookings = bookingRepository.findBookingsByUserId(foundUser.getUserId());
        foundUser.setBookings(bookings);

        for (BookingEntity booking : bookings) {
            bookingService.delete(booking.getBookingId());
        }

        foundUser.setDeletedAt(Instant.now());
        userRepository.save(foundUser);

        log.info("User was successfully deleted");
    }

    @Override
    @Transactional
    public UserFullDto changeRole(UserChangeRoleDto dto) {
        UserEntity userToUpdate = userRepository
                .findById(dto.getUserId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("UserEntity was not found by id: " + dto.getUserId()));

        userToUpdate.setRole(dto.getNewRole());

        UserEntity updatedUser = userRepository.save(userToUpdate);
        log.info("User was successfully updated");

        return userMapper.map(updatedUser);
    }

    @Override
    @Transactional
    public void changePassword(UserChangePasswordDto dto) {
        UserEntity userToUpdate = userRepository
                .findById(dto.getUserId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("UserEntity was not found by id: " + dto.getUserId()));

        throwIfNotCurrentUserOrAdmin(userToUpdate.getEmail());

        if (!passwordEncoder.matches((dto.getOldPassword() + userToUpdate.getEmail()), userToUpdate.getPassword() )) {
            throw new WrongUserPasswordException("Wrong password");
        }

        userToUpdate.setPassword(passwordEncoder.encode(dto.getNewPassword() + userToUpdate.getEmail()));
        userRepository.save(userToUpdate);
        log.info("User was successfully updated");
    }

    @Override
    @Transactional
    public void block(Integer userId) {
        UserEntity userToUpdate = userRepository
                .findById(userId)
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("UserEntity was not found by id: " + userId));

        userToUpdate.setBlocked(true);
        userRepository.save(userToUpdate);

        log.info("User was successfully updated");
    }

    private boolean currentUserIsAdmin() {
        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getAuthorities()
                .stream()
                .anyMatch(ga -> ga.getAuthority().equals("ROLE_" + Role.ADMIN.name()));
    }

    private void throwIfNotCurrentUserOrAdmin(String email) {
        if (!currentUserIsAdmin()) {
            String currentUserEmail = SecurityContextHolder.getContext().getAuthentication().getName();
            if (!currentUserEmail.equals(email)) {
                throw new RuntimeException("Can't update! Has no permission!");
            }
        }
    }
}

