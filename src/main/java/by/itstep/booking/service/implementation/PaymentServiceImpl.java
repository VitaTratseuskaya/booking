package by.itstep.booking.service.implementation;

import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.repository.UserRepository;
import by.itstep.booking.security.AuthenticationService;
import by.itstep.booking.service.PaymentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {

    private final UserRepository userRepository;
    private final AuthenticationService authenticationService;

    @Override
    @Transactional
    public void balanceReplenishment(Integer userId, double amount) {

        UserEntity user = userRepository
                .findById(userId)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + userId));

        double replenishment = user.getWallet() + amount;
        user.setWallet(replenishment);

        userRepository.save(user);
        log.info("User balance successfully updated");
    }
}
