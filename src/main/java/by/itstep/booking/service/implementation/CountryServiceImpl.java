package by.itstep.booking.service.implementation;

import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.mapper.CountryMapper;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.service.CountryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class CountryServiceImpl implements CountryService {

    private final CountryMapper countryMapper;
    private final CountryRepository countryRepository;
    private final HotelRepository hotelRepository;


    @Override
    @Transactional
    public CountryFullDto create(CountryCreateDto dto) {
        CountryEntity countryToSave = countryMapper.map(dto);
        countryToSave.setName(dto.getName());

        CountryEntity savedCountry = countryRepository.save(countryToSave);
        log.info("Country was successfully created");

        return countryMapper.map(savedCountry);
    }

    @Override
    @Transactional
    public CountryFullDto update(CountryUpdateDto dto) {
        CountryEntity countryForUpdate = countryRepository
                .findById(dto.getCountryId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("UserEntity was not found by id: " + dto.getCountryId()));

        countryForUpdate.setCountryId(dto.getCountryId());
        countryForUpdate.setName(dto.getName());

        CountryEntity updatedCountry = countryRepository.save(countryForUpdate);
        log.info("Country was successfully updated");
        return countryMapper.map(updatedCountry);
    }

    @Override
    @Transactional
    public CountryFullDto findById(Integer id) {
        CountryEntity country = countryRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));

        log.info("Found user " + country);
        return countryMapper.map(country);
    }

    @Override
    @Transactional
    public Page<CountryFullDto> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<CountryEntity> foundCountries = countryRepository.findAll(pageable);
        Page<CountryFullDto> dtos = foundCountries.map(entity -> countryMapper.map(entity));
        log.info("Found countries -> " + foundCountries.getNumberOfElements());
        return dtos;
    }

    @Override
    @Transactional
    public void delete(Integer id) {
    CountryEntity foundCountry = countryRepository
            .findById(id)
            .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));

    foundCountry.setDeletedAt(Instant.now());
    countryRepository.save(foundCountry);

    log.info("Country was successfully deleted");
    }
}
