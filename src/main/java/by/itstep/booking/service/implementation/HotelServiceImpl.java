package by.itstep.booking.service.implementation;

import by.itstep.booking.dto.hotel.*;
import by.itstep.booking.entity.*;
import by.itstep.booking.exception.AppEntityNotFoundException;
import by.itstep.booking.mapper.HotelMapper;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.repository.RoomRepository;
import by.itstep.booking.service.BookingService;
import by.itstep.booking.service.HotelService;
import by.itstep.booking.service.RoomService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class HotelServiceImpl implements HotelService {

    private final HotelMapper hotelMapper;
    private final HotelRepository hotelRepository;
    private final CountryRepository countryRepository;
    private final RoomRepository roomRepository;
    private final RoomService roomService;
    private final BookingService bookingService;

    @Override
    @Transactional
    public HotelFullDto create(HotelCreateDto dto) {
        HotelEntity hotelToSave = hotelMapper.map(dto);

        CountryEntity country = countryRepository
                .findById(dto.getCountryId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("UserEntity was not found by id: " + dto.getCountryId()));
        hotelToSave.setCountry(country);

        hotelToSave.setPrice(dto.getPrice());
        hotelToSave.setRating(dto.getRating());
        hotelToSave.setStars(dto.getStars());
        hotelToSave.setSingleRoom(dto.getSingleRoom());
        hotelToSave.setDoubleRoom(dto.getDoubleRoom());
        hotelToSave.setTripleRoom(dto.getTripleRoom());
        hotelToSave.setQuadrupleRoom(dto.getQuadrupleRoom());

        HotelEntity savedHotel = hotelRepository.save(hotelToSave);
        log.info("Hotel was successfully created");

        return hotelMapper.map(savedHotel);
    }

    @Override
    @Transactional
    public HotelFullDto update(HotelUpdateDto dto) {
        HotelEntity hotelForUpdate = hotelRepository
                .findById(dto.getHotelId())
                .orElseThrow(() -> new AppEntityNotFoundException
                        ("UserEntity was not found by id: " + dto.getHotelId()));

        hotelForUpdate.setHotelId(dto.getHotelId());
        hotelForUpdate.setName(dto.getName());
        hotelForUpdate.setPrice(dto.getPrice());
        hotelForUpdate.setRating(dto.getRating());
        hotelForUpdate.setSingleRoom(dto.getSingleRoom());
        hotelForUpdate.setDoubleRoom(dto.getDoubleRoom());
        hotelForUpdate.setTripleRoom(dto.getTripleRoom());
        hotelForUpdate.setQuadrupleRoom(dto.getQuadrupleRoom());

        HotelEntity updatedHotel = hotelRepository.save(hotelForUpdate);
        log.info("Hotel was successfully updated");

        return hotelMapper.map(updatedHotel);
    }

    @Override
    @Transactional
    public HotelFullDto findById(Integer id) {
        HotelEntity hotel = hotelRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));

        log.info("Found hotel " + hotel);
        return hotelMapper.map(hotel);
    }

    @Override
    @Transactional
    public Page<HotelFullDto> findAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<HotelEntity> foundHotels = hotelRepository.findAll(pageable);
        Page<HotelFullDto> dtos = foundHotels.map(entity -> hotelMapper.map(entity));
        log.info("Found hotels -> " + dtos.getNumberOfElements());
        return dtos;
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        HotelEntity hotel = hotelRepository
                .findById(id)
                .orElseThrow(() -> new AppEntityNotFoundException("UserEntity was not found by id: " + id));

        List<RoomEntity> rooms = roomRepository.findHotelWithRoom(hotel.getHotelId());
        hotel.setRooms(rooms);

        for (RoomEntity roomEntity : rooms) {
            for (BookingEntity booking : roomEntity.getBookings()) {
                bookingService.delete(booking.getBookingId());
            }
            roomService.delete(roomEntity.getRoomId());
        }

        hotel.setDeletedAt(Instant.now());
        hotelRepository.save(hotel);

        log.info("Hotel was successfully deleted");

    }

    @Override
    @Transactional
     public List<HotelShortDto> findAllHotelInCountry(HotelFoundDto dto) {

        List<HotelEntity> hotels = hotelRepository.findAllHotelInCountry(dto.getFinishDate(), dto.getStartDate(),
                dto.getCountryId(), dto.getQuantityBed());

        log.info("Found hotels " + hotels.size() + " in " + dto.getCountryId());

        return hotelMapper.mapToShort(hotels);
    }
}
