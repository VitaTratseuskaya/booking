package by.itstep.booking.service;

import by.itstep.booking.dto.user.UserFullDto;

public interface PaymentService {


    void balanceReplenishment(Integer userId, double amount);

}
