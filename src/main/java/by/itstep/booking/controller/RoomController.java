package by.itstep.booking.controller;

import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomShortDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import by.itstep.booking.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RoomController {

    @Autowired
    private RoomService roomService;

    @PostMapping("/rooms")
    public RoomFullDto create(@RequestBody RoomCreateDto dto) {
        return roomService.create(dto);
    }

    @PutMapping("/rooms")
    public RoomFullDto update(@RequestBody RoomUpdateDto dto) {
        return roomService.update(dto);
    }

    @GetMapping("/rooms/{id}")
    public RoomFullDto findById(@PathVariable Integer id) {
        return roomService.findById(id);
    }

    @GetMapping("/rooms")
    public Page<RoomFullDto> findAll(@RequestParam int page, @RequestParam int size) {
        return roomService.findAll(page, size);
    }

    @DeleteMapping("/rooms/{id}")
    public void delete(@PathVariable Integer id) {
        roomService.delete(id);
    }



}
