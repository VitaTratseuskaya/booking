package by.itstep.booking.controller;

import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.booking.BookingFullDto;
import by.itstep.booking.dto.booking.BookingUpdateDto;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookingController {

    @Autowired
    private BookingService bookingService;

    @PostMapping("/bookings")
    public BookingFullDto create(@RequestBody BookingCreateDto dto) {
        return bookingService.create(dto);
    }

    @GetMapping("/bookings/{id}")
    public BookingFullDto findById(@PathVariable Integer id) {
        return bookingService.findById(id);
    }

    @GetMapping("/bookings")
    public Page<BookingFullDto> findAll(@RequestParam int page, @RequestParam int size) {
        return bookingService.findAll(page, size);
    }

    @DeleteMapping("/bookings/{id}")
    public void delete(@PathVariable Integer id) {
        bookingService.delete(id);
    }

    @PutMapping("/bookings/{bookingId}")
    public void deleteBooking(@PathVariable Integer bookingId) {
        bookingService.deleteBooking(bookingId);
    }
}
