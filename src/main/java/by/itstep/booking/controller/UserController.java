package by.itstep.booking.controller;

import by.itstep.booking.dto.user.*;
import by.itstep.booking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    private UserService userService;


    @PostMapping("/users")
    public UserFullDto create(@RequestBody UserCreateDto dto) {
        return userService.create(dto);
    }

    @PutMapping("/users")
    public UserFullDto update(@RequestBody UserUpdateDto dto) {
        return userService.update(dto);
    }

    @GetMapping("/users/{id}")
    public UserFullDto findById(@PathVariable Integer id) {
        return userService.findById(id);
    }

    @GetMapping("/users")
    public Page<UserFullDto> findAll(@RequestParam int page, @RequestParam int size) {
        return userService.findAll(page, size);
    }

    @DeleteMapping("/users/{id}")
    public void delete(@PathVariable Integer id) {
        userService.delete(id);
    }

    @PutMapping("/users/role")
    public void changeRole(@RequestBody UserChangeRoleDto dto) {
        userService.changeRole(dto);
    }

    @PutMapping("/users/password")
    public void changePassword(@RequestBody UserChangePasswordDto dto) {
        userService.changePassword(dto);
    }

    @PutMapping(path = "/users/{id}/block")
    public void blocked(
            @PathVariable Integer id,
            @RequestHeader("Authorization") String authorization)
            throws Exception {
        userService.block(id);
    }
}
