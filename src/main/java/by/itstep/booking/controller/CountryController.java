package by.itstep.booking.controller;

import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.dto.country.CountryShortDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CountryController {

    @Autowired
    private CountryService countryService;

    @PostMapping("/countries")
    public CountryFullDto create(@RequestBody CountryCreateDto dto) {
        return  countryService.create(dto);
    }

    @PutMapping("/countries")
    public CountryFullDto update(@RequestBody CountryUpdateDto dto) {
        return countryService.update(dto);
    }

    @GetMapping("/countries/{id}")
    public CountryFullDto findById(@PathVariable Integer id) {
        return countryService.findById(id);
    }

    @GetMapping("/countries")
    public Page<CountryFullDto> findAll(@RequestParam int page, @RequestParam int size) {
        return countryService.findAll(page, size);
    }

    @DeleteMapping("/countries/{id}")
    public void delete(@PathVariable Integer id) {
        countryService.delete(id);
    }


}
