package by.itstep.booking.controller;

import by.itstep.booking.dto.hotel.*;
import by.itstep.booking.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class HotelController {

    @Autowired
    private HotelService hotelService;

    @PostMapping("/hotels")
    public HotelFullDto create(@RequestBody HotelCreateDto dto) {
        return hotelService.create(dto);
    }

    @PutMapping("/hotels")
    public HotelFullDto update(@RequestBody HotelUpdateDto dto) {
        return hotelService.update(dto);
    }

    @GetMapping("/hotels/{id}")
    public HotelFullDto findById(@PathVariable Integer id) {
        return hotelService.findById(id);
    }

    @GetMapping("/hotels")
    public Page<HotelFullDto> findAll(@RequestParam int page, @RequestParam int size) {
        return hotelService.findAll(page, size);
    }

    @DeleteMapping("/hotels/{id}")
    public void delete(@PathVariable Integer id) {
        hotelService.delete(id);
    }

    @PostMapping("/hotels/freeHotel")
    public List<HotelShortDto> findAllHotelInCountry(@RequestBody HotelFoundDto dto) {
       return hotelService.findAllHotelInCountry(dto);
    }
}
