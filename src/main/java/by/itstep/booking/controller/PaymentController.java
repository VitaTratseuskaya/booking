package by.itstep.booking.controller;

import by.itstep.booking.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @PutMapping("/users/{userId}/amount/{amount}")
    public void balanceReplenishment(@PathVariable  Integer userId, @RequestParam double amount){
        paymentService.balanceReplenishment(userId, amount);
    }
}
