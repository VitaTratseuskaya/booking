package by.itstep.booking.entity.enums;

public enum Role {

    USER, ADMIN
}
