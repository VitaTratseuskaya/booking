package by.itstep.booking.dto.user;

import by.itstep.booking.entity.enums.Role;
import com.sun.istack.NotNull;
import lombok.Data;

@Data
public class UserChangeRoleDto {

    @NotNull
    private Integer userId;

    @NotNull
    private Role newRole;

}
