package by.itstep.booking.dto.user;

import by.itstep.booking.dto.booking.BookingFullDto;
import by.itstep.booking.entity.enums.Role;
import lombok.Data;

import java.util.List;

@Data
public class UserFullDto {

    private Integer userId;
    private Role role;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;
    private Double wallet;

    private List<BookingFullDto> bookings;
}
