package by.itstep.booking.dto.user;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class UserUpdateDto {

    @NotNull
    private Integer userId;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @Size(min = 7, max = 20)
    private String phone;

}
