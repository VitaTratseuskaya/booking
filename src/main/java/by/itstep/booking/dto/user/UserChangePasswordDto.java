package by.itstep.booking.dto.user;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class UserChangePasswordDto {

    @NotNull
    private Integer userId;

    @NotNull
    private String oldPassword;

    @NotBlank
    @Size(min = 8)
    private String newPassword;
}
