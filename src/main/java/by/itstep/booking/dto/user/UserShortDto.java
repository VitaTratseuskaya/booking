package by.itstep.booking.dto.user;

import lombok.Data;

@Data
public class UserShortDto {

    private Integer userId;
    private String firstName;
    private String lastName;

}
