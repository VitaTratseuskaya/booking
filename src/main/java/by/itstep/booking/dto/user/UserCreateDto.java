package by.itstep.booking.dto.user;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class UserCreateDto {

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @Size(min = 7, max = 20)
    private String phone;

    @Email(message = "Email can not be null")
    private String email;

    @Size(min = 8)
    @NotBlank
    private String password;

}
