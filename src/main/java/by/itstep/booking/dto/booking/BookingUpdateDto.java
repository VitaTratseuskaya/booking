package by.itstep.booking.dto.booking;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Future;
import java.time.Instant;

@Data
public class BookingUpdateDto {

    @NotNull
    private Integer bookingId;

    @Temporal(TemporalType.DATE)
    private Instant startDate;

    @Future
    private Instant finishDate;
}
