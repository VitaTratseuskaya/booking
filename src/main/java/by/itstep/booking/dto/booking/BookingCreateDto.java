package by.itstep.booking.dto.booking;

import by.itstep.booking.dto.user.UserShortDto;
import by.itstep.booking.entity.UserEntity;
import lombok.Data;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import java.time.Instant;

@Data
public class BookingCreateDto {

    @Temporal(TemporalType.DATE)
    private Instant startDate;

    @Future
    private Instant finishDate;

    @NotBlank
    private Integer quantityBed;;

    @NotBlank
    private Integer hotelId;

}
