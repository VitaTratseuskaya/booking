package by.itstep.booking.dto.booking;

import by.itstep.booking.dto.room.RoomShortDto;
import by.itstep.booking.dto.user.UserShortDto;
import lombok.Data;
import java.time.Instant;


@Data
public class BookingFullDto {

    private Integer bookingId;
    private Instant startDate;
    private Instant finishDate;

    private UserShortDto user;
    private RoomShortDto room;

}
