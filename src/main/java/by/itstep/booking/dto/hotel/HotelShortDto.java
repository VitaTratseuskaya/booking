package by.itstep.booking.dto.hotel;

import lombok.Data;

@Data
public class HotelShortDto {

    private Integer hotelId;
    private String name;
    private double price;
}
