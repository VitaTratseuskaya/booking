package by.itstep.booking.dto.hotel;

import by.itstep.booking.dto.country.CountryShortDto;
import by.itstep.booking.dto.room.RoomShortDto;
import lombok.Data;

import java.util.List;

@Data
public  class HotelFullDto {

    private Integer hotelId;
    private String name;
    private Double price;
    private Integer stars;
    private Double rating;
    private Integer singleRoom;
    private Integer doubleRoom;
    private Integer tripleRoom;
    private Integer quadrupleRoom;

    private CountryShortDto country;
    private List<RoomShortDto> rooms;
}
