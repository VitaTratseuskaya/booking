package by.itstep.booking.dto.hotel;

import lombok.Data;

import java.time.Instant;

@Data
public class HotelFoundDto {

    private Integer countryId;
    private Integer quantityBed;
    private Instant startDate;
    private Instant finishDate;

}
