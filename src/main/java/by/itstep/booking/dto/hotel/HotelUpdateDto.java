package by.itstep.booking.dto.hotel;

import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.*;

@Data
public class HotelUpdateDto {

    @NotBlank
    private Integer hotelId;

    @Column(name = "name")
    private String name;

    @NotBlank
    private Double price;

    @NotBlank
    @Min(value = 1)
    @Max(value = 5)
    private Integer stars;

    @NotBlank
    @DecimalMin(value = "0.1")
    @DecimalMax(value = "10.0")
    private Double rating;

    @NotBlank
    private Integer singleRoom;

    @NotBlank
    private Integer doubleRoom;

    @NotBlank
    private Integer tripleRoom;

    @NotBlank
    private Integer quadrupleRoom;

}
