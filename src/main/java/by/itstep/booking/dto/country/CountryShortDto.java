package by.itstep.booking.dto.country;

import lombok.Data;

@Data
public class CountryShortDto {

    private Integer countryId;
    private String name;
}
