package by.itstep.booking.dto.country;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CountryUpdateDto {

    @NotNull
    private Integer countryId;

    @NotBlank
    private String name;

}
