package by.itstep.booking.dto.country;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class CountryCreateDto {

    @NotBlank
    private String name;
}
