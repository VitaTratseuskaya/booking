package by.itstep.booking.dto.country;

import by.itstep.booking.dto.hotel.HotelShortDto;
import lombok.Data;

import java.util.List;

@Data
public class CountryFullDto {

    private Integer countryId;
    private String name;

    private List<HotelShortDto> hotels;
}
