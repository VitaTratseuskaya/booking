package by.itstep.booking.dto.room;

import lombok.Data;

@Data
public class RoomFullDto {

    private Integer roomId;
    private Integer quantityBed;
    private Integer number;
    private Double price;

    private RoomShortDto hotel;

}
