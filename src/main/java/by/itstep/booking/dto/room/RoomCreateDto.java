package by.itstep.booking.dto.room;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
public class RoomCreateDto {

    @NotBlank
    @Min(value = 1)
    @Max(value = 4)
    private Integer quantityBed;

    @NotBlank
    private Integer number;

    @NotBlank
    private Integer hotelId;
}
