package by.itstep.booking.dto.room;

import by.itstep.booking.dto.hotel.HotelShortDto;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RoomShortDto {

    @NotNull
    private Integer roomId;

    @NotNull
    private Integer number;

    @NotNull
    private HotelShortDto hotel;
}
