package by.itstep.booking.dto.room;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class RoomUpdateDto {

    @NotNull
    private Integer roomId;

    @NotBlank
    private Integer quantityBed;

    @NotBlank
    private Integer number;

}
