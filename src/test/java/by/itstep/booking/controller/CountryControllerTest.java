package by.itstep.booking.controller;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.util.helper.DataBaseCountryHelper;
import by.itstep.booking.util.helper.DataBaseUserHelper;
import by.itstep.booking.util.helper.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static by.itstep.booking.entity.enums.Role.ADMIN;
import static by.itstep.booking.entity.enums.Role.USER;
import static by.itstep.booking.util.GenerationUtil.prepareCountryCreateDto;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CountryControllerTest extends BookingApplicationTests {

    @Autowired
    private DataBaseUserHelper dataBaseUserHelper;
    @Autowired
    private DataBaseCountryHelper dataBaseCountryHelper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void create_happyPath() throws Exception {
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        CountryCreateDto country = prepareCountryCreateDto();
        //when
        mockMvc.perform(request(POST, "/countries")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(country)))
                .andExpect(status().isOk())
                .andReturn();

    }

    @Test
    public void create_whenNotAdmin() throws Exception {
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(USER);
        String token = jwtHelper.createToken(currentUser.getEmail());

        CountryCreateDto country = prepareCountryCreateDto();
        //when
        mockMvc.perform(request(POST, "/countries")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(country)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void create_whenNotAuthenticate() throws Exception {
        CountryCreateDto country = prepareCountryCreateDto();
        //when
        mockMvc.perform(request(POST, "/countries")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(country)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void update_happyPath() throws Exception {
        //given
        UserEntity existingUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(existingUser.getEmail());

        CountryEntity existingCountry = dataBaseCountryHelper.addCountryToDataBase();

        CountryUpdateDto country = new CountryUpdateDto();
        country.setCountryId(existingCountry.getCountryId());
        country.setName(FAKER.address().country());

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/countries/" )
                .param("id", country.getCountryId() + "")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(country)))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void update_whenNotAdmin() throws Exception {
        //given
        UserEntity existingUser = dataBaseUserHelper.addUserToDataBase(USER);
        String token = jwtHelper.createToken(existingUser.getEmail());

        CountryEntity existingCountry = dataBaseCountryHelper.addCountryToDataBase();

        CountryUpdateDto country = new CountryUpdateDto();
        country.setCountryId(existingCountry.getCountryId());
        country.setName(FAKER.address().country());

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/countries/" )
                .param("id", country.getCountryId() + "")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(country)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void update_whenNotAuthenticate() throws Exception {
        //given
        CountryEntity existingCountry = dataBaseCountryHelper.addCountryToDataBase();

        CountryUpdateDto country = new CountryUpdateDto();
        country.setCountryId(existingCountry.getCountryId());
        country.setName(FAKER.address().country());

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/countries/" )
                .param("id", country.getCountryId() + "")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(country)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void findById_happyPath() throws Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        CountryEntity country = dataBaseCountryHelper.addCountryToDataBase();

        //when
        mockMvc.perform(request(GET, "/countries/" + country.getCountryId())
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void findById_whenNotAuthenticate() throws Exception {
        //given
        CountryEntity country = dataBaseCountryHelper.addCountryToDataBase();

        //when
        mockMvc.perform(request(GET, "/countries/" + country.getCountryId()))
                .andExpect(status().isForbidden())
                .andReturn();
        }

    @Test
    public void findAll_happyPath() throws Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        mockMvc.perform(request(GET, "/countries")
                .param("page", "0")
                .param("size", "30")
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size").isNotEmpty())
                .andReturn();
    }

    @Test
    public void findAll_whenNotAuthenticate() throws Exception {
        //when
        mockMvc.perform(request(GET, "/countries")
                .param("page", "0")
                .param("size", "30"))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void delete_happyPath() throws Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        CountryEntity country = dataBaseCountryHelper.addCountryToDataBase();

        //when
        mockMvc.perform(request(DELETE, "/countries/" + country.getCountryId())
                .header("Authorization", token)
                .content(objectMapper.writeValueAsString(country)))
                .andExpect(status().isOk());
    }

    @Test
    public void delete_whenNotAdmin() throws Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(USER);
        String token = jwtHelper.createToken(currentUser.getEmail());

        CountryEntity country = dataBaseCountryHelper.addCountryToDataBase();

        //when
        mockMvc.perform(request(DELETE, "/countries/" + country.getCountryId())
                .header("Authorization", token)
                .content(objectMapper.writeValueAsString(country)))
                .andExpect(status().isForbidden());
    }

    @Test
    public void delete_whenNotAuthenticate() throws Exception {
        CountryEntity country = dataBaseCountryHelper.addCountryToDataBase();

        //when
        mockMvc.perform(request(DELETE, "/countries/" + country.getCountryId())
                .content(objectMapper.writeValueAsString(country)))
                .andExpect(status().isForbidden());
    }
}
