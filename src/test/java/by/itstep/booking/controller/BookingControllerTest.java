package by.itstep.booking.controller;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.entity.*;
import by.itstep.booking.repository.*;
import by.itstep.booking.util.helper.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import static by.itstep.booking.entity.enums.Role.USER;
import static by.itstep.booking.entity.enums.Role.ADMIN;
import static by.itstep.booking.util.GenerationUtil.*;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BookingControllerTest extends BookingApplicationTests {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private DataBaseUserHelper dataBaseUserHelper;
    @Autowired
    private DataBaseRoomHelper dataBaseRoomHelper;
    @Autowired
    private DataBaseHotelHelper dataBaseHotelHelper;
    @Autowired
    private DataBaseBookingHelper dataBaseBookingHelper;
    @Autowired
    private DataBaseCountryHelper dataBaseCountryHelper;

    @Test
    public void create_happyPath() throws Exception {
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        UserEntity user = prepareUser();
        userRepository.save(user);

        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = prepareRoom(hotel);
        roomRepository.save(room);

        BookingCreateDto booking = prepareBookingCreateDto(hotel);

        //when
        mockMvc.perform(request(POST, "/bookings")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(booking)))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void create_whenNotAuthenticate() throws Exception {
        UserEntity user = prepareUser();
        userRepository.save(user);

        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = prepareRoom(hotel);
        roomRepository.save(room);

        BookingCreateDto booking = prepareBookingCreateDto(hotel);

        //when
        mockMvc.perform(request(POST, "/bookings")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(booking)))
                .andExpect(status().isForbidden())
                .andReturn();

    }

    @Test
    public void findById_happyPath() throws Exception {
        UserEntity user = prepareUser();
        userRepository.save(user);

        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = prepareRoom(hotel);
        roomRepository.save(room);

        BookingEntity booking = dataBaseBookingHelper.addBookingToDataBase(user, room);

        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        mockMvc.perform(request(GET, "/bookings/" + booking.getBookingId())
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void findById_whenNotAuthenticate() throws Exception {
        UserEntity user = prepareUser();
        userRepository.save(user);

        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = prepareRoom(hotel);
        roomRepository.save(room);

        BookingEntity booking = dataBaseBookingHelper.addBookingToDataBase(user, room);

        //when
        mockMvc.perform(request(GET, "/bookings/" + booking.getBookingId()))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void findAll_happyPath() throws Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        mockMvc.perform(request(GET, "/bookings")
                .param("page", "0")
                .param("size", "30")
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size").isNotEmpty())
                .andReturn();
    }

    @Test
    public void findAll_whenNotAuthenticate() throws Exception {
        mockMvc.perform(request(GET, "/bookings")
                .param("page", "0")
                .param("size", "30"))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void delete_happyPath() throws Exception {
        //given
        UserEntity user = prepareUser();
        userRepository.save(user);

        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = prepareRoom(hotel);
        roomRepository.save(room);

        BookingEntity booking = dataBaseBookingHelper.addBookingToDataBase(user, room);

        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        mockMvc.perform(request(DELETE, "/bookings/" + booking.getBookingId())
                .header("Authorization", token)
                .content(objectMapper.writeValueAsString(booking)))
                .andExpect(status().isOk());
    }

    @Test
    public void delete_whenNotAuthenticate() throws Exception {
        UserEntity user = prepareUser();
        userRepository.save(user);

        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = prepareRoom(hotel);
        roomRepository.save(room);

        BookingEntity booking = dataBaseBookingHelper.addBookingToDataBase(user, room);

        //when
        mockMvc.perform(request(DELETE, "/bookings/" + booking.getBookingId())
                .content(objectMapper.writeValueAsString(booking)))
                .andExpect(status().isForbidden());
    }

    @Test
    public void delete_whenNotAdmin() throws Exception {
        UserEntity user = prepareUser();
        userRepository.save(user);

        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = prepareRoom(hotel);
        roomRepository.save(room);

        BookingEntity booking = dataBaseBookingHelper.addBookingToDataBase(user, room);

        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(USER);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        mockMvc.perform(request(DELETE, "/bookings/" + booking.getBookingId())
                .header("Authorization", token)
                .content(objectMapper.writeValueAsString(booking)))
                .andExpect(status().isForbidden());
    }
}
