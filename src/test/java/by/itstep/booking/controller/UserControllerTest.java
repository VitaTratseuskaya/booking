package by.itstep.booking.controller;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.user.*;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.enums.Role;
import by.itstep.booking.repository.UserRepository;
import by.itstep.booking.util.helper.JwtHelper;
import by.itstep.booking.util.helper.DataBaseUserHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static by.itstep.booking.entity.enums.Role.ADMIN;
import static by.itstep.booking.util.GenerationUtil.prepareUserCreateDto;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest extends BookingApplicationTests {

    @Autowired
    private DataBaseUserHelper dataBaseUserHelper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;



    @Test
    public void create_happyPath() throws Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());
        UserCreateDto dto = prepareUserCreateDto();
        //when
        MvcResult mvcResult = mockMvc.perform(request(POST, "/users")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);

        //then
        Assertions.assertNotNull(foundUser.getUserId());
        Assertions.assertEquals(Role.USER, foundUser.getRole());
    }

    @Test
    public void create_whenNotAuthenticated() throws Exception {
        //given
        UserCreateDto dto = prepareUserCreateDto();

        //when
        mockMvc.perform(request(POST, "/users")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isForbidden());
    }

    @Test
    public void create_whenNotAdmin() throws Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(Role.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());
        UserCreateDto createDto = prepareUserCreateDto();

        //when
        mockMvc.perform(request(POST, "/users")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(createDto)))
                .andExpect(status().isOk());
    }

    @Test
    public void update_happyPath() throws Exception {
        //given
        UserEntity existingUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(existingUser.getEmail());

        UserUpdateDto userUpdate = new UserUpdateDto();
        userUpdate.setUserId(existingUser.getUserId());
        userUpdate.setFirstName("update");
        userUpdate.setLastName("update");
        userUpdate.setPhone("update");

        //when
        MvcResult mvcResult = mockMvc.perform(request(PUT, "/users")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(userUpdate)))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);

        //then
        Assertions.assertEquals(foundUser.getUserId(), userUpdate.getUserId());
        Assertions.assertEquals(foundUser.getFirstName(), userUpdate.getFirstName());
        Assertions.assertEquals(foundUser.getLastName(), userUpdate.getLastName());
        Assertions.assertEquals(foundUser.getPhone(), userUpdate.getPhone());
    }

    @Test
    public void update_whenNotAuthenticated() throws Exception {
        //given
        UserEntity user = new UserEntity();

        UserUpdateDto userUpdate = new UserUpdateDto();
        userUpdate.setUserId(user.getUserId());
        userUpdate.setFirstName("update");
        userUpdate.setLastName("update");
        userUpdate.setPhone("update");

        //when
        mockMvc.perform(request(PUT, "/users")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(userUpdate)))
                .andExpect(status().isForbidden());
    }

    @Test
    public void update_whenNotAdmin() throws Exception {
        //given
        UserEntity existingUser = dataBaseUserHelper.addUserToDataBase(Role.USER);
        String token = jwtHelper.createToken(existingUser.getEmail());

        UserUpdateDto userUpdate = new UserUpdateDto();
        userUpdate.setUserId(existingUser.getUserId());
        userUpdate.setFirstName("update");
        userUpdate.setLastName("update");
        userUpdate.setPhone("update");

        //when
        mockMvc.perform(request(PUT, "/users")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(userUpdate)))
                .andExpect(status().isForbidden());
    }

    @Test
    public void findById_happyPath() throws Exception {
        //given
        UserEntity existingUser = dataBaseUserHelper.addUserToDataBase(Role.USER);

        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        MvcResult mvcResult = mockMvc.perform(request(GET, "/users/" + existingUser.getUserId())
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = mvcResult.getResponse().getContentAsByteArray();
        UserFullDto foundUser = objectMapper.readValue(bytes, UserFullDto.class);

        //then
        Assertions.assertEquals(existingUser.getUserId(), foundUser.getUserId());
    }

    @Test
    public void findById_whenNotAuthenticated() throws Exception {
        //given
        UserEntity existingUser = dataBaseUserHelper.addUserToDataBase(Role.USER);
        //when
        mockMvc.perform(request(GET, "/users/" + existingUser.getUserId()))
                .andExpect(status().isForbidden());
    }

    @Test
    public void findById_NotFound() throws Exception {
        //given
        UserEntity existingUser = new UserEntity();

        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());
        //when
        mockMvc.perform(request(GET, "/users" + existingUser.getUserId())
                .header("Authorization", token))
                .andExpect(status().isNotFound());

    }

    @Test
    public void findAll_happyPath() throws Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        mockMvc.perform(request(GET, "/users")
                .param("page", "0")
                .param("size", "30")
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size").isNotEmpty())
                .andReturn();
    }

    @Test
    public void findAll_whenNotAuthenticate() throws Exception {
        mockMvc.perform(request(GET, "/users"))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void delete_happyPath() throws Exception {
        //given
        UserEntity existingUser = dataBaseUserHelper.addUserToDataBase(Role.USER);

        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        mockMvc.perform(request(DELETE, "/users/" + existingUser.getUserId())
                .header("Authorization", token)
                .content(objectMapper.writeValueAsString(existingUser)))
                .andExpect(status().isOk());
    }

    @Test
    public void delete_whenNotAdmin() throws Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(Role.USER);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        mockMvc.perform(request(DELETE, "/users/" + currentUser.getUserId())
                .header("Authorization", token)
                .content(objectMapper.writeValueAsString(currentUser)))
                .andExpect(status().isForbidden());
    }

    @Test
    public void delete_whenNotAuthenticate() throws Exception {
        UserEntity existingUser = dataBaseUserHelper.addUserToDataBase(Role.USER);
        //when
        mockMvc.perform(request(DELETE, "/users/" + existingUser.getUserId())
                .content(objectMapper.writeValueAsString(existingUser)))
                .andExpect(status().isForbidden());
    }

    @Test
    public void changeRole_happyPath() throws Exception {
        UserEntity user = dataBaseUserHelper.addUserToDataBase(Role.USER);
        UserEntity existingUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(existingUser.getEmail());

        UserChangeRoleDto userForUpdate = new UserChangeRoleDto();
        userForUpdate.setUserId(user.getUserId());
        userForUpdate.setNewRole(ADMIN);

        //when
        mockMvc.perform(request(PUT, "/users/role/")
                .param("role","ADMIN")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(userForUpdate)))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void changeRole_whenNotAdmin() throws Exception {
        UserEntity user = dataBaseUserHelper.addUserToDataBase(Role.USER);
        String token = jwtHelper.createToken(user.getEmail());

        UserChangeRoleDto userForUpdate = new UserChangeRoleDto();
        userForUpdate.setUserId(user.getUserId());
        userForUpdate.setNewRole(ADMIN);

        //when
        mockMvc.perform(request(PUT, "/users/role/")
                .param("role","ADMIN")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(userForUpdate)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void changeRole_whenNotAuthenticate() throws Exception {
        UserEntity user = dataBaseUserHelper.addUserToDataBase(Role.USER);

        UserChangeRoleDto userForUpdate = new UserChangeRoleDto();
        userForUpdate.setUserId(user.getUserId());
        userForUpdate.setNewRole(ADMIN);

        //when
        mockMvc.perform(request(PUT, "/users/role/")
                .param("role","ADMIN")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(userForUpdate)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void changePassword_happyPath() throws Exception {
        UserEntity existingUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(existingUser.getEmail());
        System.out.println("1" + existingUser.getPassword());
        existingUser.setPassword(passwordEncoder.encode("12345678update@ru"));

        mockMvc.perform(request(PUT, "/users/")
                .param("password", passwordEncoder.encode("12345678update@ru"))
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(existingUser)))
                .andExpect(status().isOk())
                .andReturn();

        System.out.println("2" + existingUser.getPassword());
    }

    @Test
    public void changePassword_whenNotAuthenticate() throws Exception {
        UserEntity existingUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
     //   String token = jwtHelper.createToken(existingUser.getEmail());

        existingUser.setPassword(passwordEncoder.encode("12345678update@ru"));

        mockMvc.perform(request(PUT, "/users/")
                .param("password", passwordEncoder.encode("12345678update@ru"))
               // .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(existingUser)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

}
