package by.itstep.booking.controller;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.dto.user.UserCreateDto;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.util.helper.DataBaseHotelHelper;
import by.itstep.booking.util.helper.DataBaseUserHelper;
import by.itstep.booking.util.helper.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static by.itstep.booking.entity.enums.Role.ADMIN;
import static by.itstep.booking.entity.enums.Role.USER;
import static by.itstep.booking.util.GenerationUtil.*;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class HotelControllerTest extends BookingApplicationTests {

    @Autowired
    private DataBaseHotelHelper dataBaseHotelHelper;
    @Autowired
    private DataBaseUserHelper dataBaseUserHelper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private HotelRepository hotelRepository;


    @Test
    public void create_happyPath() throws Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        UserCreateDto dto = prepareUserCreateDto(); CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelCreateDto hotel = prepareHotelCreateDto(country);

        //when
        MvcResult mvcResult = mockMvc.perform(request(POST, "/hotels")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(hotel)))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void create_whenNotAdmin() throws Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(USER);
        String token = jwtHelper.createToken(currentUser.getEmail());

        UserCreateDto dto = prepareUserCreateDto(); CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelCreateDto hotel = prepareHotelCreateDto(country);

        //when
        MvcResult mvcResult = mockMvc.perform(request(POST, "/hotels")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(hotel)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void create_whenNotAuthenticate() throws Exception {
        //given
        UserCreateDto dto = prepareUserCreateDto(); CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelCreateDto hotel = prepareHotelCreateDto(country);

        //when
        MvcResult mvcResult = mockMvc.perform(request(POST, "/hotels")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(hotel)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void update_happyPath() throws Exception {
        //given
        UserEntity user = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(user.getEmail());

        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        HotelUpdateDto dto = new HotelUpdateDto();
        dto.setHotelId(hotel.getHotelId());
        dto.setName(FAKER.name().nameWithMiddle());
        dto.setRating((double) FAKER.number().numberBetween(1, 10));
        dto.setPrice((double)FAKER.number().numberBetween(1, 1000));
        dto.setStars(hotel.getStars());
        dto.setSingleRoom(FAKER.number().numberBetween(1, 100));
        dto.setDoubleRoom(FAKER.number().numberBetween(1, 100));
        dto.setTripleRoom(FAKER.number().numberBetween(1, 100));
        dto.setQuadrupleRoom(FAKER.number().numberBetween(1, 100));

        //when
        mockMvc.perform(request(PUT, "/hotels")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void update_whenNotAdmin() throws Exception {
        //given
        UserEntity user = dataBaseUserHelper.addUserToDataBase(USER);
        String token = jwtHelper.createToken(user.getEmail());

        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        HotelUpdateDto dto = new HotelUpdateDto();
        dto.setHotelId(hotel.getHotelId());
        dto.setName(FAKER.name().nameWithMiddle());
        dto.setRating((double) FAKER.number().numberBetween(1, 10));
        dto.setPrice((double)FAKER.number().numberBetween(1, 1000));
        dto.setStars(hotel.getStars());
        dto.setSingleRoom(FAKER.number().numberBetween(1, 100));
        dto.setDoubleRoom(FAKER.number().numberBetween(1, 100));
        dto.setTripleRoom(FAKER.number().numberBetween(1, 100));
        dto.setQuadrupleRoom(FAKER.number().numberBetween(1, 100));

        //when
        mockMvc.perform(request(PUT, "/hotels")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void update_whenNotAuthenticate() throws Exception {
        //given
        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        HotelUpdateDto dto = new HotelUpdateDto();
        dto.setHotelId(hotel.getHotelId());
        dto.setName(FAKER.name().nameWithMiddle());
        dto.setRating((double) FAKER.number().numberBetween(1, 10));
        dto.setPrice((double)FAKER.number().numberBetween(1, 1000));
        dto.setStars(hotel.getStars());
        dto.setSingleRoom(FAKER.number().numberBetween(1, 100));
        dto.setDoubleRoom(FAKER.number().numberBetween(1, 100));
        dto.setTripleRoom(FAKER.number().numberBetween(1, 100));
        dto.setQuadrupleRoom(FAKER.number().numberBetween(1, 100));

        //when
        mockMvc.perform(request(PUT, "/hotels")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(dto)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void findById_happyPath() throws Exception {
        //given
        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = dataBaseHotelHelper.addHotelToDataBase(country);

        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        mockMvc.perform(request(GET, "/hotels/" + hotel.getHotelId())
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void findById_whenNotAuthenticate() throws Exception{
        //given
        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = dataBaseHotelHelper.addHotelToDataBase(country);

        //when
        mockMvc.perform(request(GET, "/hotels/" + hotel.getHotelId()))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void  findAll_happyPath() throws  Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        mockMvc.perform(request(GET, "/hotels")
                .param("page", "0")
                .param("size", "30")
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size").isNotEmpty())
                .andReturn();
    }

    @Test
    public void findAll_whenNotAuthenticate() throws Exception{
        mockMvc.perform(request(GET, "/hotels")
                .param("page", "0")
                .param("size", "30"))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void delete_happyPath() throws Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = dataBaseHotelHelper.addHotelToDataBase(country);

        //when
        mockMvc.perform(request(DELETE, "/hotels/" + hotel.getHotelId())
                .header("Authorization", token)
                .content(objectMapper.writeValueAsString(hotel)))
                .andExpect(status().isOk());
    }

}


