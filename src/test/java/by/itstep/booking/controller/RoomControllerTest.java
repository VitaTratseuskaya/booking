package by.itstep.booking.controller;

import by.itstep.booking.BookingApplicationTests;
import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.repository.RoomRepository;
import by.itstep.booking.util.helper.DataBaseRoomHelper;
import by.itstep.booking.util.helper.DataBaseUserHelper;
import by.itstep.booking.util.helper.JwtHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;

import static by.itstep.booking.entity.enums.Role.ADMIN;
import static by.itstep.booking.entity.enums.Role.USER;
import static by.itstep.booking.util.GenerationUtil.*;
import static org.springframework.http.HttpMethod.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RoomControllerTest extends BookingApplicationTests {

    @Autowired
    private DataBaseUserHelper dataBaseUserHelper;
    @Autowired
    private DataBaseRoomHelper dataBaseRoomHelper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private JwtHelper jwtHelper;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private RoomRepository roomRepository;


    @Test
    public void create_happyPath() throws Exception {
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomCreateDto room = prepareRoomCreateDto(hotel);

        //when
        mockMvc.perform(request(POST, "/rooms")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void create_whenNotAdmin() throws Exception {
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(USER);
        String token = jwtHelper.createToken(currentUser.getEmail());

        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomCreateDto room = prepareRoomCreateDto(hotel);

        //when
        mockMvc.perform(request(POST, "/rooms")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void create_whenNotAuthenticate() throws Exception {
        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomCreateDto room = prepareRoomCreateDto(hotel);

        //when
        mockMvc.perform(request(POST, "/rooms")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void update_happyPath() throws Exception {
        //given
        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity existingRoom = prepareRoom(hotel);
        roomRepository.save(existingRoom);

        UserEntity user = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(user.getEmail());

        RoomUpdateDto room = new RoomUpdateDto();
        room.setRoomId(existingRoom.getRoomId());
        room.setNumber(FAKER.number().numberBetween(1, 1000));
        room.setQuantityBed(FAKER.number().numberBetween(1, 4));

        //when
        mockMvc.perform(request(PUT, "/rooms")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void update_whenNotAdmin() throws Exception {
        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity existingRoom = prepareRoom(hotel);
        roomRepository.save(existingRoom);

        UserEntity user = dataBaseUserHelper.addUserToDataBase(USER);
        String token = jwtHelper.createToken(user.getEmail());

        RoomUpdateDto room = new RoomUpdateDto();
        room.setRoomId(existingRoom.getRoomId());
        room.setNumber(FAKER.number().numberBetween(1, 1000));
        room.setQuantityBed(FAKER.number().numberBetween(1, 4));

        //when
        mockMvc.perform(request(PUT, "/rooms")
                .header("Authorization", token)
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void update_whenNotAuthenticate() throws Exception {
        //given
        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity existingRoom = prepareRoom(hotel);
        roomRepository.save(existingRoom);

        RoomUpdateDto room = new RoomUpdateDto();
        room.setRoomId(existingRoom.getRoomId());
        room.setNumber(FAKER.number().numberBetween(1, 1000));
        room.setQuantityBed(FAKER.number().numberBetween(1, 4));

        //when
        mockMvc.perform(request(PUT, "/rooms")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void findById_happyPath() throws Exception {
        //given
        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = dataBaseRoomHelper.addRoomToDataBase(hotel);

        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        mockMvc.perform(request(GET, "/rooms/" + room.getRoomId())
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void findById_whenNotAuthenticate() throws Exception {
        //given
        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = dataBaseRoomHelper.addRoomToDataBase(hotel);

        //when
        mockMvc.perform(request(GET, "/rooms/" + room.getRoomId()))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void findAll_happyPath() throws Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        //when
        mockMvc.perform(request(GET, "/rooms")
                .param("page", "0")
                .param("size", "30")
                .header("Authorization", token))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size").isNotEmpty())
                .andReturn();
    }

    @Test
    public void findAll_whenNotAuthenticate() throws Exception {
        mockMvc.perform(request(GET, "/rooms")
                .param("page", "0")
                .param("size", "30"))
                .andExpect(status().isForbidden())
                .andReturn();
    }

    @Test
    public void delete_happyPath() throws Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(ADMIN);
        String token = jwtHelper.createToken(currentUser.getEmail());

        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = dataBaseRoomHelper.addRoomToDataBase(hotel);

        //when
        mockMvc.perform(request(DELETE, "/rooms/" + room.getRoomId())
                .header("Authorization", token)
                .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isOk());
    }

    @Test
    public void delete_whenNotAuthenticate() throws Exception {
        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = dataBaseRoomHelper.addRoomToDataBase(hotel);

        //when
        mockMvc.perform(request(DELETE, "/rooms/" + room.getRoomId())
                .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isForbidden());
    }

    @Test
    public void delete_whenNotAdmin() throws Exception {
        //given
        UserEntity currentUser = dataBaseUserHelper.addUserToDataBase(USER);
        String token = jwtHelper.createToken(currentUser.getEmail());

        CountryEntity country = prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = dataBaseRoomHelper.addRoomToDataBase(hotel);

        //when
        mockMvc.perform(request(DELETE, "/rooms/" + room.getRoomId())
                .header("Authorization", token)
                .content(objectMapper.writeValueAsString(room)))
                .andExpect(status().isForbidden());
    }

}
