package by.itstep.booking;

import by.itstep.booking.util.DataBaseCleaner;
import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@AutoConfigureMockMvc
public class BookingApplicationTests {

	public static final Faker FAKER = new Faker();

	@Autowired
	private DataBaseCleaner cleaner;

	/*@BeforeEach
	public void setUp() {
		cleaner.clean();
	}

	@AfterEach
	public void shutUp() {
		cleaner.clean();
	}*/
}
