package by.itstep.booking.util;

import by.itstep.booking.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DataBaseCleaner {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private BookingRepository bookingRepository;


    public void clean() {

        bookingRepository.deleteAllInBatch();
        userRepository.deleteAllInBatch();
        roomRepository.deleteAllInBatch();
        hotelRepository.deleteAllInBatch();
        countryRepository.deleteAllInBatch();
    }
}
