package by.itstep.booking.util.helper;

import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static by.itstep.booking.BookingApplicationTests.FAKER;

@Component
public class DataBaseRoomHelper {

    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private RoomRepository roomRepository;

    public RoomEntity addRoomToDataBase(HotelEntity hotel) {
        RoomEntity room = new RoomEntity();
        room.setHotel(hotel);
        room.setQuantityBed(FAKER.number().numberBetween(1, 4));
        room.setNumber(FAKER.number().numberBetween(1, 1000));
        double result = 0.0;
        if (room.getQuantityBed() == 1) {
            result = hotel.getPrice() * 1.0;
            room.setPrice(result);
        }
        if (room.getQuantityBed() == 2) {
            result = hotel.getPrice() * 1.25;
            room.setPrice(result);
        }
        if (room.getQuantityBed() == 3) {
            result = hotel.getPrice() * 1.5;
            room.setPrice(result);
        }
        if (room.getQuantityBed() == 4) {
            result = hotel.getPrice() * 1.75;
            room.setPrice(result);
        }
        return roomRepository.save(room);
    }

}
