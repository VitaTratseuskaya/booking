package by.itstep.booking.util.helper;

import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static by.itstep.booking.BookingApplicationTests.FAKER;

@Component
public class DataBaseCountryHelper {

    @Autowired
    private CountryRepository countryRepository;

    public CountryEntity addCountryToDataBase() {
        CountryEntity country = new CountryEntity();
        country.setName(FAKER.address().country());

        return countryRepository.save(country);
    }
}
