package by.itstep.booking.util.helper;
import by.itstep.booking.entity.BookingEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.repository.BookingRepository;
import by.itstep.booking.repository.RoomRepository;
import by.itstep.booking.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
public class DataBaseBookingHelper {

    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private UserRepository userRepository;

    public BookingEntity addBookingToDataBase(UserEntity user, RoomEntity room){
        BookingEntity booking = new BookingEntity();
        booking.setUser(user);
        booking.setRoom(room);
        booking.setStartDate(Instant.now());
        booking.setFinishDate(Instant.now().plusSeconds(86400));
        booking.setDeletedAt(null);

        return bookingRepository.save(booking);
    }
}
