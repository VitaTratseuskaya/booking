package by.itstep.booking.util.helper;

import by.itstep.booking.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JwtHelper {

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    public String createToken(String email) {
    return "Bearer " + jwtTokenProvider.createToken(email);
    }
}
