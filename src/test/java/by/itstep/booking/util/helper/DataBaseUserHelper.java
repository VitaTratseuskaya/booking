package by.itstep.booking.util.helper;

import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.enums.Role;
import by.itstep.booking.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import static by.itstep.booking.BookingApplicationTests.FAKER;

@Component
public class DataBaseUserHelper {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserEntity addUserToDataBase(Role role) {
        UserEntity user = new UserEntity();
        user.setEmail(FAKER.internet().emailAddress());
        user.setPassword(FAKER.internet().password());
        user.setRole(role);
        user.setWallet((double)FAKER.number().randomDigitNotZero());
        user.setFirstName(FAKER.name().firstName());
        user.setLastName(FAKER.name().lastName());
        user.setBlocked(false);
        user.setPhone(FAKER.phoneNumber().phoneNumber());

        return userRepository.save(user);
    }
}
