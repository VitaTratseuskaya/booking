package by.itstep.booking.util.helper;

import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import static by.itstep.booking.BookingApplicationTests.FAKER;

@Component
public class DataBaseHotelHelper {

    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private HotelRepository hotelRepository;

    public HotelEntity addHotelToDataBase(CountryEntity country) {
        HotelEntity hotel = new HotelEntity();
        hotel.setCountry(country);
        hotel.setStars(FAKER.number().numberBetween(1, 5));
        hotel.setName(FAKER.name().nameWithMiddle());
        hotel.setPrice((double)FAKER.number().numberBetween(10, 1000));
        hotel.setRating((double)FAKER.number().numberBetween(1, 10));
        hotel.setSingleRoom(FAKER.number().numberBetween(1, 100));
        hotel.setDoubleRoom(FAKER.number().numberBetween(1, 100));
        hotel.setTripleRoom(FAKER.number().numberBetween(1, 100));
        hotel.setQuadrupleRoom(FAKER.number().numberBetween(1, 100));

        return hotelRepository.save(hotel);
    }
}
