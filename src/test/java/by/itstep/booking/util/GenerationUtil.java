package by.itstep.booking.util;

import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.user.UserCreateDto;
import by.itstep.booking.entity.*;
import by.itstep.booking.entity.enums.Role;
import by.itstep.booking.repository.*;
import by.itstep.booking.service.RoomService;

import java.time.Instant;

import static by.itstep.booking.BookingApplicationTests.FAKER;

public class GenerationUtil {

    public static UserRepository userRepository;
    public static RoomRepository roomRepository;
    public static HotelRepository hotelRepository;
    public static CountryRepository countryRepository;
    public static BookingRepository bookingRepository;

    public static UserEntity prepareUser() {
        UserEntity user = new UserEntity();

        user.setUserId(1234512135);
        user.setFirstName(FAKER.name().firstName());
        user.setLastName((FAKER.name().lastName()));
        user.setEmail(FAKER.internet().emailAddress());
        user.setPassword(FAKER.internet().password());
        user.setPhone(FAKER.phoneNumber().cellPhone());
        user.setRole(Role.ADMIN);
        user.setWallet(0.0);
        user.setBlocked(false);

        return user;
    }
    public static UserCreateDto prepareUserCreateDto() {
        UserCreateDto user = new UserCreateDto();

        user.setFirstName(FAKER.name().firstName());
        user.setLastName(FAKER.name().lastName());
        user.setPhone(FAKER.phoneNumber().phoneNumber());
        user.setEmail(FAKER.internet().emailAddress());
        user.setPassword(FAKER.internet().password());

        return user;
    }

    public static RoomEntity prepareRoom(HotelEntity hotel) {
        RoomEntity room = new RoomEntity();

        room.setHotel(hotel);
        room.setNumber(FAKER.number().randomDigit());
        room.setPrice((double) FAKER.number().numberBetween(1, 1000));
        room.setQuantityBed(FAKER.number().numberBetween(1, 4));
        room.setDeletedAt(null);
        double result = 0.0;
        if (room.getQuantityBed() == 1) {
            result = hotel.getPrice() * 1.0;
            room.setPrice(result);
        }
        if (room.getQuantityBed() == 2) {
            result = hotel.getPrice() * 1.25;
            room.setPrice(result);
        }
        if (room.getQuantityBed() == 3) {
            result = hotel.getPrice() * 1.5;
            room.setPrice(result);
        }
        if (room.getQuantityBed() == 4) {
            result = hotel.getPrice() * 1.75;
            room.setPrice(result);
        }
    return room;
    }
    public static RoomCreateDto prepareRoomCreateDto(HotelEntity hotel) {
        RoomCreateDto room = new RoomCreateDto();

        room.setHotelId(hotel.getHotelId());
        room.setNumber(FAKER.number().randomDigit());
        room.setQuantityBed((FAKER.number().randomDigit()));

        return room;
    }

    public static HotelEntity prepareHotel(CountryEntity country) {
        HotelEntity hotel = new HotelEntity();

        hotel.setCountry(country);
        hotel.setName(FAKER.name().name());
        hotel.setPrice((double) FAKER.number().numberBetween(10, 1000));
        hotel.setSingleRoom(FAKER.number().randomDigitNotZero());
        hotel.setDoubleRoom(FAKER.number().randomDigitNotZero());
        hotel.setTripleRoom(FAKER.number().randomDigitNotZero());
        hotel.setQuadrupleRoom(FAKER.number().randomDigitNotZero());
        hotel.setStars(FAKER.number().randomDigitNotZero());
        hotel.setRating((double) FAKER.number().randomDigitNotZero());

        return hotel;
    }
    public static HotelCreateDto prepareHotelCreateDto(CountryEntity country) {
        HotelCreateDto hotel = new HotelCreateDto();

        hotel.setCountryId(country.getCountryId());
        hotel.setName(FAKER.name().name());
        hotel.setRating((double) FAKER.number().numberBetween(1, 10));
        hotel.setPrice((double) FAKER.number().numberBetween(1, 1000));
        hotel.setStars(FAKER.number().numberBetween(1, 5));
        hotel.setSingleRoom(FAKER.number().numberBetween(1, 100));
        hotel.setDoubleRoom(FAKER.number().numberBetween(1, 100));
        hotel.setTripleRoom(FAKER.number().numberBetween(1, 100));
        hotel.setQuadrupleRoom(FAKER.number().numberBetween(1, 100));

        return hotel;
    }

    public static CountryEntity prepareCountry() {
        CountryEntity country = new CountryEntity();

        country.setName(FAKER.address().country());

        return country;
    }
    public static CountryCreateDto prepareCountryCreateDto() {
        CountryCreateDto country = new CountryCreateDto();

        country.setName(FAKER.address().country());

        return country;
    }

    public static BookingEntity prepareBooking(UserEntity user, RoomEntity room) {
        BookingEntity booking = new BookingEntity();

        booking.setStartDate(Instant.now());
        booking.setFinishDate(Instant.now().plusSeconds(864000));
        booking.setUser(user);
        booking.setRoom(room);

        return booking;
    }
    public static BookingCreateDto prepareBookingCreateDto(HotelEntity hotel) {
        BookingCreateDto booking = new BookingCreateDto();
        System.out.println("=");
        booking.setStartDate(Instant.now());
        booking.setFinishDate(Instant.now().plusSeconds(864000));
        booking.setHotelId(hotel.getHotelId());
        booking.setQuantityBed(FAKER.number().numberBetween(1, 4));

        return booking;
    }

}
