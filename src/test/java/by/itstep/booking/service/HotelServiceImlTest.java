package by.itstep.booking.service;

import by.itstep.booking.dto.hotel.HotelCreateDto;
import by.itstep.booking.dto.hotel.HotelFullDto;
import by.itstep.booking.dto.hotel.HotelShortDto;
import by.itstep.booking.dto.hotel.HotelUpdateDto;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.entity.HotelEntity;
import by.itstep.booking.entity.RoomEntity;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.repository.HotelRepository;
import by.itstep.booking.repository.RoomRepository;
import by.itstep.booking.util.DataBaseCleaner;
import by.itstep.booking.util.GenerationUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@SpringBootTest
public class HotelServiceImlTest {

    @Autowired
    private HotelService hotelService;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private DataBaseCleaner cleaner;

    @BeforeEach
    public void setUp() {
        cleaner.clean();
    }

    @AfterEach
    public void shutDown() {
        cleaner.clean();
    }


    @Test
    @Transactional
    public void create_happyPath() {
        //given
        CountryEntity country = GenerationUtil.prepareCountry();
        countryRepository.save(country);

        HotelCreateDto hotelToSave = GenerationUtil.prepareHotelCreateDto(country);
        //when
        HotelFullDto createdHotel = hotelService.create(hotelToSave);
        //than
        Assertions.assertNotNull(createdHotel);
    }

    @Test
    @Transactional
    public void update_happyPath() {
        //given
        HotelFullDto existingHotel = addHotelDtoToDataBase();

        HotelUpdateDto hotelUpdate = new HotelUpdateDto();
        hotelUpdate.setHotelId(existingHotel.getHotelId());
        hotelUpdate.setName("update");
        hotelUpdate.setPrice(1.1);
        hotelUpdate.setRating(1.1);
        hotelUpdate.setStars(1);
        hotelUpdate.setSingleRoom(1);
        hotelUpdate.setDoubleRoom(1);
        hotelUpdate.setTripleRoom(1);
        hotelUpdate.setQuadrupleRoom(1);

        //when
        HotelFullDto updatedHotel = hotelService.update(hotelUpdate);

        //then
        Assertions.assertEquals(existingHotel.getHotelId(), updatedHotel.getHotelId());

        HotelEntity afterUpdate = hotelRepository.getById(existingHotel.getHotelId());
        Assertions.assertEquals(afterUpdate.getName(), updatedHotel.getName());
        Assertions.assertEquals(afterUpdate.getPrice(), updatedHotel.getPrice());
        Assertions.assertEquals(afterUpdate.getRating(), updatedHotel.getRating());
        Assertions.assertEquals(afterUpdate.getStars(), updatedHotel.getStars());
        Assertions.assertEquals(afterUpdate.getSingleRoom(), updatedHotel.getSingleRoom());
        Assertions.assertEquals(afterUpdate.getDoubleRoom(), updatedHotel.getDoubleRoom());
        Assertions.assertEquals(afterUpdate.getTripleRoom(), updatedHotel.getTripleRoom());
        Assertions.assertEquals(afterUpdate.getQuadrupleRoom(), updatedHotel.getQuadrupleRoom());

    }

    @Test
    @Transactional
    public void findById_happyPath() {
        //given
        HotelEntity hotel = addHotelToDataBase();
        //when
        HotelFullDto foundHotel = hotelService.findById(hotel.getHotelId());
        //then
        Assertions.assertEquals(hotel.getHotelId(), foundHotel.getHotelId());
    }

    @Test
    @Transactional
    public void findAll_happyPath() {
        //given
        addHotelToDataBase();
        //when
        Page<HotelFullDto> allHotels = hotelService.findAll(0, 1);
        //then
        Assertions.assertEquals(1, allHotels.getTotalElements());
    }

    @Test
    @Transactional
    public void delete_happyPath() {
        //given
        CountryEntity country = GenerationUtil.prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = GenerationUtil.prepareHotel(country);
        hotelRepository.save(hotel);

        //when
        hotelService.delete(hotel.getHotelId());

        //then
        Assertions.assertNotNull(hotel.getDeletedAt());
        Assertions.assertNotNull(country);
        Assertions.assertEquals(0, hotel.getRooms().size());
    }

    private HotelEntity addHotelToDataBase() {
        CountryEntity country = GenerationUtil.prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = GenerationUtil.prepareHotel(country);
        return hotelRepository.save(hotel);
    }
    private HotelFullDto addHotelDtoToDataBase() {
        CountryEntity country = GenerationUtil.prepareCountry();
        countryRepository.save(country);

        HotelCreateDto hotel = GenerationUtil.prepareHotelCreateDto(country);
        return hotelService.create(hotel);
    }

}
