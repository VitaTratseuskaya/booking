package by.itstep.booking.service;

import by.itstep.booking.dto.user.*;
import by.itstep.booking.entity.UserEntity;
import by.itstep.booking.entity.enums.Role;
import by.itstep.booking.mapper.UserMapper;
import by.itstep.booking.repository.UserRepository;
import by.itstep.booking.security.AuthenticationService;
import by.itstep.booking.security.JwtTokenProvider;
import by.itstep.booking.util.DataBaseCleaner;
import by.itstep.booking.util.GenerationUtil;
import org.apache.catalina.authenticator.FormAuthenticator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@SpringBootTest
public class UserServiceImplTest {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private DataBaseCleaner cleaner;

    @MockBean
    private AuthenticationService authenticationService;


  /*  @BeforeEach
    public void setUp() {
        cleaner.clean();
    }

    @AfterEach
    public void shutDown() {
        cleaner.clean();
    }
*/

    @Test
    @Transactional
    public void create_happyPath() {
        //given
        UserCreateDto userToSave = GenerationUtil.prepareUserCreateDto();
        //when
        UserFullDto createdUser = userService.create(userToSave);
        //then
        Assertions.assertNotNull(createdUser.getUserId());
    }

    @Test
    @Transactional
    public void update_happyPath() {
        //given
        UserEntity user = addUserToDataBase();
        Mockito.when(authenticationService.getAuthenticationUser()).thenReturn(user);

        UserUpdateDto userUpdateDto = new UserUpdateDto();
        userUpdateDto.setUserId(user.getUserId());
        userUpdateDto.setPhone("test");
        userUpdateDto.setFirstName("test");
        userUpdateDto.setLastName("test");

        //when
        UserFullDto updatedUser = userService.update(userUpdateDto);

        //then
        Assertions.assertEquals(user.getUserId(), updatedUser.getUserId());

        UserEntity afterUpdate = userRepository.getById(updatedUser.getUserId());
        Assertions.assertEquals(afterUpdate.getFirstName(), updatedUser.getFirstName());
        Assertions.assertEquals(afterUpdate.getLastName(), updatedUser.getLastName());
        Assertions.assertEquals(afterUpdate.getPhone(), updatedUser.getPhone());

    /*    UserEntity exp = addUserToDataBase();
        when(authenticationService.getAuthenticationUser()).thenReturn(exp);

        UserUpdateDto user = new UserUpdateDto();
        user.setUserId(exp.getUserId());

        UserFullDto actual = userService.update(user);
        System.out.println("actual -> " + actual);
        System.out.println("expected -> " + exp);
        Assertions.assertEquals(exp.getUserId(), actual.getUserId());
        Assertions.assertEquals(exp.getFirstName(), actual.getFirstName());
        Assertions.assertEquals(exp.getLastName(), actual.getLastName());
        Assertions.assertEquals(exp.getPhone(), actual.getPhone());*/





    }

    @Test
    @Transactional
    public void findById_happyPath() {
        //given
        UserEntity user = addUserToDataBase();
        when(authenticationService.getAuthenticationUser()).thenReturn(user);
        //when
        UserFullDto foundUser = userService.findById(user.getUserId());
        //then
        Assertions.assertEquals(user.getUserId(), foundUser.getUserId());
    }

    @Test
    @Transactional
    public void findAll_happyPath() {
        //given
        when(authenticationService.getAuthenticationUser()).thenReturn(addUserToDataBase());
        //when
        Page<UserFullDto> allUsers = userService.findAll(0,  1);
        //then
        Assertions.assertEquals(1, allUsers.getNumberOfElements());
    }

    @Test
    @Transactional
    public void updateRole_happyPath() {
        //given
        UserFullDto existingUser = addUserDtoToDataBase();

        UserChangeRoleDto userForChangeRole = new UserChangeRoleDto();
        userForChangeRole.setUserId(existingUser.getUserId());
        userForChangeRole.setNewRole(Role.ADMIN);

        UserFullDto useAfterUpdate = new UserFullDto();
        useAfterUpdate.setUserId(userForChangeRole.getUserId());
        useAfterUpdate.setRole(userForChangeRole.getNewRole());
        //when
        UserFullDto updatedUser = userService.changeRole(userForChangeRole);
        //than
        Assertions.assertEquals(updatedUser.getUserId(), useAfterUpdate.getUserId());
        Assertions.assertEquals(updatedUser.getRole(),useAfterUpdate.getRole());
    }

    @Test
    @Transactional
    public void delete_happyPath() {
        //given
        UserEntity user = GenerationUtil.prepareUser();
        when(authenticationService.getAuthenticationUser()).thenReturn(user);

        userRepository.save(user);
        //when
        userService.delete(user.getUserId());
        //then
        Assertions.assertNotNull(user.getDeletedAt());
    }

    private UserEntity addUserToDataBase() {
        UserEntity userToAdd = GenerationUtil.prepareUser();
        return userRepository.save(userToAdd);
    }
    private UserFullDto addUserDtoToDataBase() {
        UserCreateDto createDto = GenerationUtil.prepareUserCreateDto();
        return userService.create(createDto);

    }
}
