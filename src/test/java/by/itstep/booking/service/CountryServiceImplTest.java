package by.itstep.booking.service;


import by.itstep.booking.dto.country.CountryCreateDto;
import by.itstep.booking.dto.country.CountryFullDto;
import by.itstep.booking.dto.country.CountryShortDto;
import by.itstep.booking.dto.country.CountryUpdateDto;
import by.itstep.booking.entity.CountryEntity;
import by.itstep.booking.repository.CountryRepository;
import by.itstep.booking.util.DataBaseCleaner;
import by.itstep.booking.util.GenerationUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
public class CountryServiceImplTest {

    @Autowired
    private CountryService countryService;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private DataBaseCleaner cleaner;

    @BeforeEach
    public void setUp() {
        cleaner.clean();
    }
    @AfterEach
    public void shutDown() {
        cleaner.clean();
    }


    @Test
    @Transactional
    public void create_happyPath() {
        //given
        CountryCreateDto countryToSave = GenerationUtil.prepareCountryCreateDto();
        //when
        CountryFullDto createdCountry = countryService.create(countryToSave);
        //than
        Assertions.assertNotNull(createdCountry);
    }

    @Test
    @Transactional
    public void update_happyPath() {
        //given
        CountryFullDto existingCountry = addCountryDtoToDataBase();

        CountryUpdateDto countryUpdate = new CountryUpdateDto();
        countryUpdate.setCountryId(existingCountry.getCountryId());
        countryUpdate.setName("update");
        //when
        CountryFullDto updatedCountry = countryService.update(countryUpdate);
        //then
        Assertions.assertEquals(existingCountry.getCountryId(), updatedCountry.getCountryId());

        CountryEntity afterUpdate = countryRepository.getById(existingCountry.getCountryId());
        Assertions.assertEquals(afterUpdate.getName(), updatedCountry.getName());
    }

    @Test
    @Transactional
    public void findById_happyPath() {
        //given
        CountryEntity country = addCountryToDataBase();
        //when
        CountryFullDto foundCountry = countryService.findById(country.getCountryId());
        //then
        Assertions.assertEquals(country.getCountryId(), foundCountry.getCountryId());
    }

    @Test
    @Transactional
    public void findAll_happyPath() {
        //given
        addCountryToDataBase();
        //when
        Page<CountryFullDto> allCountries = countryService.findAll(0, 1);
        //then
        Assertions.assertEquals(1, allCountries.getTotalElements());
    }

    @Test
    @Transactional
    public void delete_happyPath(){
        //given
        CountryEntity country = GenerationUtil.prepareCountry();
        countryRepository.save(country);
        //when
        countryService.delete(country.getCountryId());
        //then
        Assertions.assertNotNull(country.getDeletedAt());
    }


    private CountryEntity addCountryToDataBase() {
        CountryEntity countryToAdd = GenerationUtil.prepareCountry();
        return countryRepository.save(countryToAdd);
    }
    private CountryFullDto addCountryDtoToDataBase() {
        CountryCreateDto createDto = GenerationUtil.prepareCountryCreateDto();
        return countryService.create(createDto);
    }
}
