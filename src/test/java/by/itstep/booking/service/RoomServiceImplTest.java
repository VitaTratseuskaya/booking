package by.itstep.booking.service;

import by.itstep.booking.dto.room.RoomCreateDto;
import by.itstep.booking.dto.room.RoomFullDto;
import by.itstep.booking.dto.room.RoomShortDto;
import by.itstep.booking.dto.room.RoomUpdateDto;
import by.itstep.booking.entity.*;
import by.itstep.booking.repository.*;
import by.itstep.booking.util.DataBaseCleaner;
import by.itstep.booking.util.GenerationUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SpringBootTest
public class RoomServiceImplTest {

    @Autowired
    private RoomService roomService;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DataBaseCleaner cleaner;

    @BeforeEach
    public void setUp() {
        cleaner.clean();
    }

    @AfterEach
    public void shutDown() {
        cleaner.clean();
    }


    @Test
    @Transactional
    public void create_happyPath() {
        //given
        CountryEntity country = GenerationUtil.prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = GenerationUtil.prepareHotel(country);
        hotelRepository.save(hotel);

        RoomCreateDto roomToSave = GenerationUtil.prepareRoomCreateDto(hotel);
        //when
        RoomFullDto createdRoom = roomService.create(roomToSave);
        //than
        Assertions.assertNotNull(createdRoom.getRoomId());
    }

    @Test
    @Transactional
    public void update_happyPath() {
        //given
        RoomFullDto existingRoom = addRoomDtoToDataBase();

        RoomUpdateDto roomUpdate = new RoomUpdateDto();
        roomUpdate.setRoomId(existingRoom.getRoomId());
        roomUpdate.setNumber(1);
        roomUpdate.setQuantityBed(1);

        //when
        RoomFullDto updatedRoom = roomService.update(roomUpdate);
        //then
        Assertions.assertEquals(existingRoom.getRoomId(), updatedRoom.getRoomId());

        RoomEntity afterUpdate = roomRepository.getById(existingRoom.getRoomId());
        Assertions.assertEquals(afterUpdate.getQuantityBed(), updatedRoom.getQuantityBed());
        Assertions.assertEquals(afterUpdate.getNumber(), updatedRoom.getNumber());

    }

    @Test
    @Transactional
    public void findById_happyPath() {
        //given
        RoomEntity room = addRoomToDataBase();
        //when
        RoomFullDto foundRoom = roomService.findById(room.getRoomId());
        //then
        Assertions.assertEquals(room.getRoomId(), foundRoom.getRoomId());
    }

    @Test
    @Transactional
    public void findAll_happyPath() {
        //given
        addRoomToDataBase();
        //when
        Page<RoomFullDto> allRooms = roomService.findAll(0, 1);
        //then
        Assertions.assertEquals(1, allRooms.getTotalElements());
    }

    @Test
    @Transactional
    public void delete_happyPath() {
        //given
        CountryEntity country = GenerationUtil.prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = GenerationUtil.prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = GenerationUtil.prepareRoom(hotel);
        roomRepository.save(room);

        //when
        roomService.delete(room.getRoomId());

        //then
        Assertions.assertNotNull(room.getDeletedAt());
        Assertions.assertNotNull(country);
        Assertions.assertNotNull(hotel);
        Assertions.assertEquals(0, room.getBookings().size());
    }


    private RoomEntity addRoomToDataBase() {
        CountryEntity country = GenerationUtil.prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = GenerationUtil.prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = GenerationUtil.prepareRoom(hotel);
       ;
        return roomRepository.save(room);
    }
    private RoomFullDto addRoomDtoToDataBase() {
        CountryEntity country = GenerationUtil.prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = GenerationUtil.prepareHotel(country);
        hotelRepository.save(hotel);

        RoomCreateDto room = GenerationUtil.prepareRoomCreateDto(hotel);
        return roomService.create(room);
    }


}
