package by.itstep.booking.service;

import by.itstep.booking.dto.booking.BookingCreateDto;
import by.itstep.booking.dto.booking.BookingFullDto;
import by.itstep.booking.dto.booking.BookingUpdateDto;
import by.itstep.booking.entity.*;
import by.itstep.booking.mapper.BookingMapper;
import by.itstep.booking.repository.*;
import by.itstep.booking.util.DataBaseCleaner;
import by.itstep.booking.util.GenerationUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class BookingServiceImplTest {

    @Autowired
    private BookingService bookingService;
    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private HotelRepository hotelRepository;
    @Autowired
    private CountryRepository countryRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private DataBaseCleaner cleaner;
    @Autowired
    private BookingMapper bookingMapper;

    @BeforeEach
    public void setUp() {
        cleaner.clean();
    }

    @AfterEach
    public void shutDown() {
        cleaner.clean();
    }

    @Test
    @Transactional
    public void findById_happyPath() {
        //given
        BookingEntity booking = addBookingToDataBase();
        //when
        BookingFullDto foundBooking = bookingService.findById(booking.getBookingId());
        //then
        Assertions.assertEquals(booking.getBookingId(), foundBooking.getBookingId());
    }

    @Test
    @Transactional
    public void findAll_happyPath() {
        //given
        addBookingToDataBase();
        //when
        Page<BookingFullDto> allBookings = bookingService.findAll(0,1);
        //then
        Assertions.assertEquals(1, allBookings.getTotalElements());
    }

    @Test
    @Transactional
    public void delete_happyPath() {
        //given
        CountryEntity country = GenerationUtil.prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = GenerationUtil.prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = GenerationUtil.prepareRoom(hotel);
        roomRepository.save(room);

        UserEntity user = GenerationUtil.prepareUser();
        userRepository.save(user);

        BookingEntity booking = GenerationUtil.prepareBooking(user, room);
        bookingRepository.save(booking);
        //when
        bookingService.delete(booking.getBookingId());
        //then
        Assertions.assertNotNull(booking.getDeletedAt());
        Assertions.assertNotNull(user);
        Assertions.assertNotNull(room);
    }


    private BookingEntity addBookingToDataBase() {
        CountryEntity country = GenerationUtil.prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = GenerationUtil.prepareHotel(country);
        hotelRepository.save(hotel);

        RoomEntity room = GenerationUtil.prepareRoom(hotel);
        roomRepository.save(room);

        UserEntity user = GenerationUtil.prepareUser();
        userRepository.save(user);

        BookingEntity booking = GenerationUtil.prepareBooking(user, room);
        return bookingRepository.save(booking);
    }
    private BookingFullDto addBookingDtoToDataBase() {
        CountryEntity country = GenerationUtil.prepareCountry();
        countryRepository.save(country);

        HotelEntity hotel = GenerationUtil.prepareHotel(country);
        hotelRepository.save(hotel);

        UserEntity user = GenerationUtil.prepareUser();
        userRepository.save(user);
        Integer userId = user.getUserId();

        BookingCreateDto booking = GenerationUtil.prepareBookingCreateDto(hotel);
        return bookingService.create(booking);
    }

}
